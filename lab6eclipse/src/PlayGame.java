// 
// Decompiled by Procyon v0.5.36
// 

package lab6eclipse;

public class PlayGame
{
    public static void main(final String[] args) {
        final RpsGame oneGame = new RpsGame();
        final String result = oneGame.playRound("paper");
        System.out.println(result);
        System.out.println("Wins: " + oneGame.getWins());
        System.out.println("Ties: " + oneGame.getTies());
        System.out.println("Losses: " + oneGame.getLosses());
    }
}
