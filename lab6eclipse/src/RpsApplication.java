// 
// Decompiled by Procyon v0.5.36
// 

package lab6eclipse;

import javafx.scene.paint.Paint;
import javafx.scene.paint.Color;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import javafx.scene.control.TextField;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.control.Button;
import javafx.scene.Group;
import javafx.stage.Stage;
import javafx.application.Application;

public class RpsApplication extends Application
{
    private RpsGame gameOne;
    
    public void start(final Stage stage) {
        final Group root = new Group();
        this.gameOne = new RpsGame();
        final Button rock = new Button("rock");
        final Button paper = new Button("paper");
        final Button scissors = new Button("scissors");
        final HBox buttons = new HBox();
        buttons.getChildren().addAll((Object[])new Node[] { (Node)rock, (Node)scissors, (Node)paper });
        final TextField message = new TextField("Welcome");
        message.setPrefWidth(200.0);
        final TextField wins = new TextField("wins: ");
        final TextField losses = new TextField("losses: ");
        final TextField ties = new TextField("ties: ");
        final HBox textFields = new HBox();
        textFields.getChildren().addAll((Object[])new Node[] { (Node)message, (Node)wins, (Node)losses, (Node)ties });
        final VBox overall = new VBox();
        overall.getChildren().addAll((Object[])new Node[] { (Node)buttons, (Node)textFields });
        root.getChildren().add((Object)overall);
        final RpsChoice rockChoice = new RpsChoice(message, wins, losses, ties, "rock", this.gameOne);
        rock.setOnAction((EventHandler)rockChoice);
        final RpsChoice paperChoice = new RpsChoice(message, wins, losses, ties, "paper", this.gameOne);
        paper.setOnAction((EventHandler)paperChoice);
        final RpsChoice scissorsChoice = new RpsChoice(message, wins, losses, ties, "scissors", this.gameOne);
        scissors.setOnAction((EventHandler)scissorsChoice);
        final Scene scene = new Scene((Parent)root, 650.0, 300.0);
        scene.setFill((Paint)Color.BLUE);
        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);
        stage.show();
    }
    
    public static void main(final String[] args) {
        Application.launch(args);
    }
}
