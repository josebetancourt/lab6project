// 
// Decompiled by Procyon v0.5.36
// 

package lab6eclipse;

import javafx.event.Event;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RpsChoice implements EventHandler<ActionEvent>
{
    private TextField message;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String choice;
    private RpsGame gameOne;
    
    RpsChoice(final TextField message, final TextField wins, final TextField losses, final TextField ties, final String choice, final RpsGame gameOne) {
        this.message = message;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.choice = choice;
        this.gameOne = gameOne;
    }
    
    public void handle(final ActionEvent event) {
        final String gameResult = this.gameOne.playRound(this.choice);
        this.message.setText(gameResult);
        this.wins.setText("wins: " + this.gameOne.getWins());
        this.losses.setText("losses: " + this.gameOne.getLosses());
        this.ties.setText("ties: " + this.gameOne.getTies());
    }
}
