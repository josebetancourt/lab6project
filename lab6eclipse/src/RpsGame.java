//Jose Carlos Betancourt Saiz de la Mora
package lab6eclipse

import java.util.Random;

public class RpsGame {
	private int wins = 0;
	private int ties = 0;
	private int losses = 0;
	
	Random rand = new Random();
	
	public int getWins() {
		return this.wins;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public int getLosses() {
		retiurn this.losses;
	}
	
	public String playRound(String choice) {
		int randNumber = rand.nextInt(3);
		int playerChoice;
		String computerChoice = "";
		String result = "";
		
		if (choice.equals("rock")) {
			playerChoice = 0;
		}
		else if (choice.equals("scissors")) {
			playerChoice = 1;
		}
		else if (choice.equals("paper")) {
			playerChoice = 2;
		}
		else {
			playerChoice = -1;
		}
		
		if (randNumber == 0) {
			computerChoice = "rock";
		}
		else if (randNumber == 1) {
			computerChoice = "scissors";
		}
		else if (randNumber == 2) {
			computerChoice = "paper";
		}
		else {
			computerChoice = "N/A";
		}
		
		if((randNumber == 0 && playerChoice == 1) || (randNumber == 1 && playerChoice == 2) || (randNumber == 2 && playerChoice == 0)) {
			result = "Computer plays " + computerChoice + "and the computer won";
			this.losses++;
		}
		else if (randNumber == playerChoice) {
			result = "Computer plays " + computerChoice + " and it's a tie";
			this.ties++;
		}
		else {
			result = "Computer plays " + computerChoice + " and you won";
			this.wins++;
		}
		return result;
	}
	
}
